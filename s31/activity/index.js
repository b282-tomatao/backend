//Add solution here

let http = require("http");

const port = 3000;

const app = http.createServer((req, res) => {

    if(req.url == '/login') {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Welcome to the login page.');
    } else {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        res.end("I'm sorry the page you are looking for cannot be found");
    }
});

app.listen(port);

console.log(`server is successfully running`);


//Do not modify
module.exports = {app}