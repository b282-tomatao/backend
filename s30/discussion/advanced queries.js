//Advanced Queries

// Query an embedded document
db.users.find({
    contact: {
        phone: "87654321",
        email: "stephenhawking@gmail.com"
    }
});

// Query on nested field
db.users.find(
    {"contact.email": "janedoe@gmail.com"}
);

// Querying an Array with Exact Elements
db.users.find( { courses: [ "CSS", "Javascript", "Python" ] } );

// Querying an Array without regard to order
db.users.find( { courses: { $all: ["React", "Python"] } } );

// Querying an Embedded Array
db.users.insertOne({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});

db.users.find({
    namearr: 
        {
            namea: "juan"
        }
});
