const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController");

const auth = require('../auth');


router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(
        resultFromController => res.send(resultFromController)
    )
})

router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(
        resultFromController => res.send(resultFromController)
    )
})

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(
        resultFromController => res.send(resultFromController)
    )
})



// Route for retrieving user details 
router.get("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    userController.getProfile({ userId: userData.id }).then(
        resultFromController => res.send(resultFromController));
});


    // Activity
    // 1*
    // Retrieve user details
    // 1. Find the document in the database using the user's ID
    // 2. Reassign the password of the returned document to an
    // empty string
    // 3. Return the result back to the frontend


router.post("/enroll", auth.verify, (req, res) => {

    let data = {
        userId: auth.decode(req.headers.authorization).id,
        courseId: req.body.courseId
    }

    userController.enroll(data).then(
        resultFromController => res.send(resultFromController));
});







module.exports = router;