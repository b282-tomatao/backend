const express = require('express');

const router = express.Router();

const courseController = require('../controllers/courseController');

const auth = require("../auth");


// router.post('/create', (req, res) => {
//     courseController.addCourse(req.body).then(result =>{
//         resultFromCourseController => res.send(
//             resultFromCourseController
//         )
//     })
// })

// s39 activity start

router.post("/create", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});

// s39 activity end


router.get('/all', (req, res) => {
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

router.get('/active', (req, res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

router.get('/:courseId', (req, res) => {

    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

router.put('/:courseId', auth.verify, (req, res) => {

    courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// s40 ACTIVITY START

router.patch('/:courseId/archive', auth.verify, (req, res) => {

    courseController.archiveCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController));
})
// s40 ACTIVITY END

module.exports = router;