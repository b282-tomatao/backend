//Comparison Query Operators

//$gt / $gte operator
/*
	- Allows us to find documents that have field number values greater than or equal to a specified value.
	- Syntax
		db.collectionName.find({ field : { $gt : value } });
		db.collectionName.find({ field : { $gte : value } });
*/

db.users.find({age: {$gt : 50}});
db.users.find({age: {$gte : 50}});

//$lt / $lte operator
/*
	- Allows us to find documents that have field number values less than or equal to a specified value.
	- Syntax
		db.collectionName.find({ field : { $lt : value } });
		db.collectionName.find({ field : { $lte : value } });
*/
db.users.find({age: {$lt : 50}});
db.users.find({age: {$lte : 50}});